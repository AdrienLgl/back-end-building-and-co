# back-end-building-and-co

Back-end Exam 2021-2022

Julien Lemenicier / Adrien Laigle

B3 Dev

## Lancement

Pour pouvoir exécuter le projet, il vous faut créer la base de données "building" sur pgAdmin. *(cf script ci-dessous)*

Ensuite, modifier le mot de passe et le login de votre base de données dans le fichier "application.properties".

```
spring.datasource.password=${DATABASE_PASSWORD:your_password}
spring.datasource.username=${DATABASE_USERNAME:your_username}
```

## Base de données

```sql
-- Database: building

-- DROP DATABASE building;

CREATE DATABASE building
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'French_France.1252'
    LC_CTYPE = 'French_France.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
```

## Spring Boot Security

**Rôles et identifiants :**

```
ADMIN (toutes les permissions) : admin / admin

TECHNICIAN : technician / technician

MANAGER : manager / manager

RH : rh / rh

```