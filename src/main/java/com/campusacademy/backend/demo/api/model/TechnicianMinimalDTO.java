package com.campusacademy.backend.demo.api.model;

import com.campusacademy.backend.demo.model.Address;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TechnicianMinimalDTO {
    Long id;
    String firstname;
    String lastname;
    int age;
}
