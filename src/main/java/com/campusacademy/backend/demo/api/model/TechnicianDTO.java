package com.campusacademy.backend.demo.api.model;

import java.util.List;

import com.campusacademy.backend.demo.model.Address;
import com.campusacademy.backend.demo.model.Manager;
import com.campusacademy.backend.demo.model.Vehicle;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TechnicianDTO extends TechnicianMinimalDTO {
    ManagerMinimalDTO manager;
    Long adresseId;
    Long vehicleId;
    List<WorksiteMinimalDTO> worksites;

    public TechnicianDTO(Long id, String firstname, String lastname, int age, Address address,
            ManagerMinimalDTO manager, Vehicle vehicle,
            List<WorksiteMinimalDTO> worksites) {
        super(id, firstname, lastname, age);
        this.manager = manager;
        this.worksites = worksites;
        this.adresseId = address.getId();
        this.vehicleId = vehicle.getId();
    }
}
