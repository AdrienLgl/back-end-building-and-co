package com.campusacademy.backend.demo.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.parameters.RequestBody;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.campusacademy.backend.demo.api.model.VehicleDTO;
import com.campusacademy.backend.demo.model.Vehicle;
import com.campusacademy.backend.demo.services.VehicleService;

@RequiredArgsConstructor
@RestController
@RequestMapping(produces = APPLICATION_JSON_VALUE, path = "vehicle")
public class VehicleController {

    private final VehicleService vehicleService;

    @GetMapping
    public ResponseEntity<List<VehicleDTO>> getAll() {
        return ResponseEntity.ok(this.vehicleService.findAll()
                .stream()
                .map(this::map)
                .collect(Collectors.toList()));
    }

    @GetMapping(path = "{id}")
    public ResponseEntity<VehicleDTO> getById(@PathVariable Long id) {
        return this.vehicleService.findById(id)
                .map(vehicle -> ResponseEntity.ok(map(vehicle)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<VehicleDTO> create(@RequestBody VehicleDTO vehicleDTO) {

        Vehicle vehicle = new Vehicle();

        vehicle.setVehicleBrand(vehicleDTO.getVehicleBrand());
        vehicle.setVehicleLicense(vehicleDTO.getVehicleLicense());
        vehicle.setVehicleYear(vehicleDTO.getVehicleYear());

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(map(this.vehicleService.create(vehicle)));
    }

    @PutMapping(path = "{id}")
    public ResponseEntity<VehicleDTO> update(@PathVariable Long id, @RequestBody VehicleDTO vehicleDTO) {
        Optional<Vehicle> vehicleOptional = this.vehicleService.findById(id);
        if (vehicleOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        Vehicle vehicle = new Vehicle();
        vehicle.setVehicleBrand(vehicleDTO.getVehicleBrand());
        vehicle.setVehicleLicense(vehicleDTO.getVehicleLicense());
        vehicle.setVehicleYear(vehicleDTO.getVehicleYear());
        return ResponseEntity.ok(map(this.vehicleService.update(vehicle)));
    }

    @DeleteMapping(path = "{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        this.vehicleService.delete(id);
    }

    private VehicleDTO map(@NonNull Vehicle vehicle) {
        VehicleDTO vehicleDTO = new VehicleDTO(vehicle.getId(), vehicle.getVehicleBrand(), vehicle.getVehicleLicense(),
                vehicle.getVehicleYear());
        return vehicleDTO;
    }
}
