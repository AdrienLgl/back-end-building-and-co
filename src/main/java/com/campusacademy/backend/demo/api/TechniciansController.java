package com.campusacademy.backend.demo.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.parameters.RequestBody;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.campusacademy.backend.demo.api.model.AddressMinimalDTO;
import com.campusacademy.backend.demo.api.model.ManagerMinimalDTO;
import com.campusacademy.backend.demo.api.model.TechnicianDTO;
import com.campusacademy.backend.demo.api.model.WorksiteMinimalDTO;
import com.campusacademy.backend.demo.model.Address;
import com.campusacademy.backend.demo.model.Manager;
import com.campusacademy.backend.demo.model.Technician;
import com.campusacademy.backend.demo.model.Vehicle;
import com.campusacademy.backend.demo.model.Worksite;
import com.campusacademy.backend.demo.repositories.AddressRepository;
import com.campusacademy.backend.demo.repositories.ManagerRepository;
import com.campusacademy.backend.demo.repositories.VehicleRepository;
import com.campusacademy.backend.demo.services.AddressService;
import com.campusacademy.backend.demo.services.ManagerService;
import com.campusacademy.backend.demo.services.TechnicianService;
import com.campusacademy.backend.demo.services.VehicleService;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping(produces = APPLICATION_JSON_VALUE, path = "technicians")
public class TechniciansController {

    private final TechnicianService technicianService;
    private final AddressService addressService;
    private final VehicleService vehicleService;
    private final ManagerService managerService;

    @GetMapping
    public ResponseEntity<List<TechnicianDTO>> getAll() {
        return ResponseEntity.ok(this.technicianService.findAll().stream().map(this::map).collect(Collectors.toList()));
    }

    @GetMapping(path = "{id}")
    public ResponseEntity<TechnicianDTO> getById(@PathVariable Long id) {
        return this.technicianService.findById(id)
                .map(technician -> ResponseEntity.ok(map(technician)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<TechnicianDTO> create(@RequestBody TechnicianDTO technicianDTO) {
        Technician technician = new Technician();
        technician.setTechnicianAge(technicianDTO.getAge());
        technician.setTechnicianFirstname(technicianDTO.getFirstname());
        technician.setTechnicianLastname(technicianDTO.getLastname());

        Optional<Address> address = this.addressService.findById(technicianDTO.getAdresseId());
        if (address.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        Optional<Vehicle> vehicle = this.vehicleService.findById(technicianDTO.getVehicleId());
        if (vehicle.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        Optional<Manager> manager = this.managerService.findById((technicianDTO.getManager().getId()));
        if (manager.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        technician.setAddress(address.get());
        technician.setVehicle(vehicle.get());
        technician.setManager(manager.get());

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(map(this.technicianService.create(technician)));
    }

    @PutMapping(path = "{id}")
    public ResponseEntity<TechnicianDTO> update(@PathVariable Long id, @RequestBody TechnicianDTO technicianDTO) {
        Optional<Technician> technicianOptional = this.technicianService.findById(id);
        if (technicianOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        Technician technician = new Technician();
        technician.setTechnicianAge(technicianDTO.getAge());
        technician.setTechnicianFirstname(technicianDTO.getFirstname());
        technician.setTechnicianLastname(technicianDTO.getLastname());

        Optional<Address> address = this.addressService.findById(technicianDTO.getAdresseId());
        if (address.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        Optional<Vehicle> vehicle = this.vehicleService.findById(technicianDTO.getVehicleId());
        if (vehicle.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        Optional<Manager> manager = this.managerService.findById((technicianDTO.getManager().getId()));
        if (manager.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        technician.setAddress(address.get());
        technician.setVehicle(vehicle.get());
        technician.setManager(manager.get());
        return ResponseEntity.ok(map(this.technicianService.update(technician)));
    }

    @DeleteMapping(path = "{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        this.technicianService.delete(id);
    }

    private TechnicianDTO map(@NonNull Technician technician) {
        List<WorksiteMinimalDTO> worksites = map(technician.getWorksites());
        return new TechnicianDTO(technician.getId(), technician.getTechnicianFirstname(),
                technician.getTechnicianLastname(), technician.getTechnicianAge(), technician.getAddress(),
                mapManager(technician.getManager()), technician.getVehicle(), worksites);
    }

    private List<WorksiteMinimalDTO> map(List<Worksite> worksites) {
        List<WorksiteMinimalDTO> minimalDTOs = new ArrayList<WorksiteMinimalDTO>();
        for (Worksite worksite : worksites) {
            WorksiteMinimalDTO w = new WorksiteMinimalDTO(worksite.getId(),
                    worksite.getWorksiteName(), worksite.getPrice());
            minimalDTOs.add(w);
        }
        return minimalDTOs;
    }

    private ManagerMinimalDTO mapManager(Manager manager) {
        ManagerMinimalDTO m = new ManagerMinimalDTO(manager.getId(), manager.getManagerFirstname(),
                manager.getManagerLastname());
        return m;
    }

    private AddressMinimalDTO mapAddress(Address address) {
        AddressMinimalDTO a = new AddressMinimalDTO(address.getCityName(), address.getStreet(), address.getNumber());
        return a;
    }
}
