package com.campusacademy.backend.demo.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.parameters.RequestBody;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.campusacademy.backend.demo.api.model.AddressMinimalDTO;
import com.campusacademy.backend.demo.api.model.ManagerMinimalDTO;
import com.campusacademy.backend.demo.api.model.TechnicianDTO;
import com.campusacademy.backend.demo.api.model.WorksiteDTO;
import com.campusacademy.backend.demo.api.model.WorksiteMinimalDTO;
import com.campusacademy.backend.demo.model.Address;
import com.campusacademy.backend.demo.model.Manager;
import com.campusacademy.backend.demo.model.Technician;
import com.campusacademy.backend.demo.model.Vehicle;
import com.campusacademy.backend.demo.model.Worksite;
import com.campusacademy.backend.demo.repositories.AddressRepository;
import com.campusacademy.backend.demo.repositories.ManagerRepository;
import com.campusacademy.backend.demo.repositories.VehicleRepository;
import com.campusacademy.backend.demo.services.AddressService;
import com.campusacademy.backend.demo.services.ManagerService;
import com.campusacademy.backend.demo.services.TechnicianService;
import com.campusacademy.backend.demo.services.VehicleService;
import com.campusacademy.backend.demo.services.WorksiteService;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping(produces = APPLICATION_JSON_VALUE, path = "worksite")
public class WorksiteController {

    private final WorksiteService worksiteService;
    private final AddressService addressService;
    private final TechnicianService technicianService;

    @GetMapping
    public ResponseEntity<List<WorksiteDTO>> getAll() {
        return ResponseEntity.ok(this.worksiteService.findAll().stream().map(this::map).collect(Collectors.toList()));
    }

    @GetMapping(path = "{id}")
    public ResponseEntity<WorksiteDTO> getById(@PathVariable Long id) {
        return this.worksiteService.findById(id)
                .map(worksite -> ResponseEntity.ok(map(worksite)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<WorksiteDTO> create(@RequestBody WorksiteDTO worksiteDTO) {

        Worksite worksite = new Worksite();

        worksite.setPrice(worksiteDTO.getPrice());
        worksite.setWorksiteName(worksiteDTO.getName());

        Optional<Address> address = this.addressService.findById(worksite.getAddress().getId());
        if (address.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        worksite.setAddress(address.get());

        Optional<Technician> technician = this.technicianService.findById(worksite.getTechnician().getId());
        if (technician.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        worksite.setTechnician(technician.get());

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(map(this.worksiteService.create(worksite)));
    }

    @PutMapping(path = "{id}")
    public ResponseEntity<WorksiteDTO> update(@PathVariable Long id, @RequestBody WorksiteDTO worksiteDTO) {

        Optional<Worksite> worksiteOptionnal = this.worksiteService.findById(id);
        if (worksiteOptionnal.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        Worksite worksite = new Worksite();

        worksite.setPrice(worksiteDTO.getPrice());
        worksite.setWorksiteName(worksiteDTO.getName());

        Optional<Address> address = this.addressService.findById(worksite.getAddress().getId());
        if (address.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        worksite.setAddress(address.get());

        Optional<Technician> technician = this.technicianService.findById(worksite.getTechnician().getId());
        if (technician.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        worksite.setTechnician(technician.get());

        return ResponseEntity.ok(map(this.worksiteService.update(worksite)));

    }

    @DeleteMapping(path = "{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        this.worksiteService.delete(id);
    }

    private WorksiteDTO map(@NonNull Worksite worksite) {
        WorksiteDTO worksiteDTO = new WorksiteDTO(worksite.getId(), worksite.getWorksiteName(),
                worksite.getAddress().getId(), worksite.getTechnician().getId(), worksite.getPrice());
        return worksiteDTO;
    }

}
