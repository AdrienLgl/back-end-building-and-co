package com.campusacademy.backend.demo.api.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class WorksiteDTO extends WorksiteMinimalDTO {

    Long addressId;
    Long technicianId;

    public WorksiteDTO(Long id, String name, Long addressId, Long technicianId, Float price) {
        // add manager DTO minimal
        super(id, name, price);
        this.addressId = addressId;
        this.technicianId = technicianId;
    }

}
