package com.campusacademy.backend.demo.api.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class VehicleDTO {
    Long id;
    String vehicleBrand;
    String vehicleLicense;
    String vehicleYear;

    public VehicleDTO(Long id, String vehicleBrand, String vehicleLicense, String vehicleYear) {
        this.id = id;
        this.vehicleBrand = vehicleBrand;
        this.vehicleLicense = vehicleLicense;
        this.vehicleYear = vehicleYear;
    }
}
