package com.campusacademy.backend.demo.api.model;

import java.util.List;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ManagerDTO extends ManagerMinimalDTO {

    String managerFixPhone;
    String managerPortablePhone;
    List<TechnicianMinimalDTO> technicians;

    public ManagerDTO(Long id, String managerFirstname, String managerLastname, String managerFixPhone,
            String managerPortablePhone, List<TechnicianMinimalDTO> technicians) {
        super(id, managerFirstname, managerLastname);
        this.managerFixPhone = managerFixPhone;
        this.managerPortablePhone = managerPortablePhone;
        this.technicians = technicians;
    }
}
