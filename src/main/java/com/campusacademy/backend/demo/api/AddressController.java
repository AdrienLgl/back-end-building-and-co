package com.campusacademy.backend.demo.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.parameters.RequestBody;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.campusacademy.backend.demo.api.model.AddressDTO;
import com.campusacademy.backend.demo.model.Address;
import com.campusacademy.backend.demo.model.Vehicle;
import com.campusacademy.backend.demo.services.AddressService;
import com.campusacademy.backend.demo.services.VehicleService;

@RequiredArgsConstructor
@RestController
@RequestMapping(produces = APPLICATION_JSON_VALUE, path = "addresses")
public class AddressController {

    private final AddressService addressService;

    @GetMapping
    public ResponseEntity<List<AddressDTO>> getAll() {
        return ResponseEntity.ok(this.addressService.findAll()
                .stream()
                .map(this::map)
                .collect(Collectors.toList()));
    }

    @GetMapping(path = "{id}")
    public ResponseEntity<AddressDTO> getById(@PathVariable Long id) {
        return this.addressService.findById(id)
                .map(vehicle -> ResponseEntity.ok(map(vehicle)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<AddressDTO> create(@RequestBody AddressDTO addressDTO) {

        Address address = new Address();

        address.setCityName(addressDTO.getCityName());
        address.setNumber(addressDTO.getNumber());
        address.setStreet(addressDTO.getStreet());

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(map(this.addressService.create(address)));
    }

    @PutMapping(path = "{id}")
    public ResponseEntity<AddressDTO> update(@PathVariable Long id, @RequestBody AddressDTO addressDTO) {
        Optional<Address> addressOptional = this.addressService.findById(id);
        if (addressOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        Address address = new Address();
        address.setCityName(addressDTO.getCityName());
        address.setNumber(addressDTO.getNumber());
        address.setStreet(addressDTO.getStreet());
        return ResponseEntity.ok(map(this.addressService.update(address)));
    }

    @DeleteMapping(path = "{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        this.addressService.delete(id);
    }

    private AddressDTO map(@NonNull Address address) {
        AddressDTO addressDTO = new AddressDTO(address.getCityName(), address.getStreet(),
                address.getNumber());
        return addressDTO;

    }

}
