package com.campusacademy.backend.demo.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.parameters.RequestBody;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.campusacademy.backend.demo.api.model.AddressDTO;
import com.campusacademy.backend.demo.api.model.ManagerDTO;
import com.campusacademy.backend.demo.api.model.TechnicianDTO;
import com.campusacademy.backend.demo.api.model.TechnicianMinimalDTO;
import com.campusacademy.backend.demo.model.Address;
import com.campusacademy.backend.demo.model.Manager;
import com.campusacademy.backend.demo.model.Technician;
import com.campusacademy.backend.demo.model.Vehicle;
import com.campusacademy.backend.demo.services.AddressService;
import com.campusacademy.backend.demo.services.ManagerService;
import com.campusacademy.backend.demo.services.VehicleService;

@RequiredArgsConstructor
@RestController
@RequestMapping(produces = APPLICATION_JSON_VALUE, path = "manager")
public class ManagerController {

    private final ManagerService managerService;

    @GetMapping
    public ResponseEntity<List<ManagerDTO>> getAll() {
        return ResponseEntity.ok(this.managerService.findAll()
                .stream()
                .map(this::map)
                .collect(Collectors.toList()));
    }

    @GetMapping(path = "{id}")
    public ResponseEntity<ManagerDTO> getById(@PathVariable Long id) {
        return this.managerService.findById(id)
                .map(vehicle -> ResponseEntity.ok(map(vehicle)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<ManagerDTO> create(@RequestBody ManagerDTO managerDTO) {

        Manager manager = new Manager();

        manager.setManagerFirstname(managerDTO.getManagerFirstname());
        manager.setManagerLastname(managerDTO.getManagerLastname());
        manager.setManagerFixPhone(managerDTO.getManagerFixPhone());
        manager.setManagerPortablePhone(managerDTO.getManagerPortablePhone());

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(map(this.managerService.create(manager)));
    }

    @PutMapping(path = "{id}")
    public ResponseEntity<ManagerDTO> update(@PathVariable Long id, @RequestBody ManagerDTO managerDTO) {
        Optional<Manager> managerOptional = this.managerService.findById(id);
        if (managerOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        Manager manager = new Manager();
        manager.setManagerFirstname(managerDTO.getManagerFirstname());
        manager.setManagerLastname(managerDTO.getManagerLastname());
        manager.setManagerFixPhone(managerDTO.getManagerFixPhone());
        manager.setManagerPortablePhone(managerDTO.getManagerPortablePhone());

        return ResponseEntity.ok(map(this.managerService.update(manager)));
    }

    @DeleteMapping(path = "{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        this.managerService.delete(id);
    }

    private ManagerDTO map(@NonNull Manager manager) {
        ManagerDTO managerDTO = new ManagerDTO(manager.getId(), manager.getManagerFirstname(),
                manager.getManagerLastname(), manager.getManagerFixPhone(), manager.getManagerPortablePhone(),
                mapTechnician(manager.getTechnicians()));
        return managerDTO;
    }

    private List<TechnicianMinimalDTO> mapTechnician(List<Technician> technicians) {
        List<TechnicianMinimalDTO> minimalDTOs = new ArrayList<TechnicianMinimalDTO>();
        for (Technician technician : technicians) {
            TechnicianMinimalDTO t = new TechnicianMinimalDTO(technician.getId(), technician.getTechnicianFirstname(),
                    technician.getTechnicianLastname(), technician.getTechnicianAge());
            minimalDTOs.add(t);
        }
        return minimalDTOs;
    }

}
