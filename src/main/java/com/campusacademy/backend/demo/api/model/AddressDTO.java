package com.campusacademy.backend.demo.api.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AddressDTO extends AddressMinimalDTO {
    String cityName;
    String street;
    int number;

    public AddressDTO(String cityName, String street, int number) {
        super(cityName, street, number);
    }
}
