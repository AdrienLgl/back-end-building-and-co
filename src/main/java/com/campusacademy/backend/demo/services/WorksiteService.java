package com.campusacademy.backend.demo.services;

import java.util.List;
import java.util.Optional;

import com.campusacademy.backend.demo.model.Worksite;
import com.campusacademy.backend.demo.repositories.WorksiteRepository;

import org.springframework.stereotype.Component;

@Component
public class WorksiteService {

    private WorksiteRepository worksiteRepository;

    public WorksiteService(WorksiteRepository worksiteRepository) {
        this.worksiteRepository = worksiteRepository;
    }

    public List<Worksite> findAll() {
        return this.worksiteRepository.findAll();
    }

    public Optional<Worksite> findById(Long id) {
        return this.worksiteRepository.findById(id);
    }

    public Worksite create(Worksite worksite) {
        worksite.setId(null);
        return this.worksiteRepository.save(worksite);
    }

    public Worksite update(Worksite worksite) {
        return this.worksiteRepository.save(worksite);
    }

    public void delete(Long id) {
        this.worksiteRepository.deleteById(id);
    }

}
