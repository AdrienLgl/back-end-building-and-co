package com.campusacademy.backend.demo.services;

import java.util.List;
import java.util.Optional;

import com.campusacademy.backend.demo.model.Manager;
import com.campusacademy.backend.demo.repositories.ManagerRepository;

import org.springframework.stereotype.Component;

@Component
public class ManagerService {

    private ManagerRepository managerRepository;

    public ManagerService(ManagerRepository managerRepository) {
        this.managerRepository = managerRepository;
    }

    public List<Manager> findAll() {
        return this.managerRepository.findAll();
    }

    public Optional<Manager> findById(Long id) {
        return this.managerRepository.findById(id);
    }

    public Manager create(Manager manager) {
        manager.setId(null);
        return this.managerRepository.save(manager);
    }

    public Manager update(Manager manager) {
        return this.managerRepository.save(manager);
    }

    public void delete(Long id) {
        this.managerRepository.deleteById(id);
    }

}
