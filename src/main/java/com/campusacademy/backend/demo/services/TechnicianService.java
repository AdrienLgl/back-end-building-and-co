package com.campusacademy.backend.demo.services;

import java.util.List;
import java.util.Optional;

import com.campusacademy.backend.demo.model.Technician;
import com.campusacademy.backend.demo.repositories.TechnicianRepository;

import org.springframework.stereotype.Component;

@Component
public class TechnicianService {

    private TechnicianRepository technicianRepository;

    public TechnicianService(TechnicianRepository technicianRepository) {
        this.technicianRepository = technicianRepository;
    }

    public List<Technician> findAll() {
        return this.technicianRepository.findAll();
    }

    public Optional<Technician> findById(Long id) {
        return this.technicianRepository.findById(id);
    }

    public Technician create(Technician technician) {
        technician.setId(null);
        return this.technicianRepository.save(technician);
    }

    public Technician update(Technician technician) {
        return this.technicianRepository.save(technician);
    }

    public void delete(Long id) {
        this.technicianRepository.deleteById(id);
    }
}
