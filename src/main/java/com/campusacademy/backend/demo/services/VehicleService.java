package com.campusacademy.backend.demo.services;

import java.util.List;
import java.util.Optional;

import com.campusacademy.backend.demo.model.Vehicle;
import com.campusacademy.backend.demo.repositories.VehicleRepository;

import org.springframework.stereotype.Component;

@Component
public class VehicleService {
    private VehicleRepository vehicleRepository;

    public VehicleService(VehicleRepository vehicleRepository) {
        this.vehicleRepository = vehicleRepository;
    }

    public List<Vehicle> findAll() {
        return this.vehicleRepository.findAll();
    }

    public Optional<Vehicle> findById(Long id) {
        return this.vehicleRepository.findById(id);
    }

    public Vehicle create(Vehicle vehicle) {
        vehicle.setId(null);
        return this.vehicleRepository.save(vehicle);
    }

    public Vehicle update(Vehicle vehicle) {
        return this.vehicleRepository.save(vehicle);
    }

    public void delete(Long id) {
        this.vehicleRepository.deleteById(id);
    }
}
