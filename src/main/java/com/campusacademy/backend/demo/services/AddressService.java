package com.campusacademy.backend.demo.services;

import java.util.List;
import java.util.Optional;

import com.campusacademy.backend.demo.model.Address;
import com.campusacademy.backend.demo.repositories.AddressRepository;

import org.springframework.stereotype.Component;

@Component
public class AddressService {

    private AddressRepository addressRepository;

    public AddressService(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    public List<Address> findAll() {
        return this.addressRepository.findAll();
    }

    public Optional<Address> findById(Long id) {
        return this.addressRepository.findById(id);
    }

    public Address create(Address address) {
        address.setId(null);
        return this.addressRepository.save(address);
    }

    public Address update(Address address) {
        return this.addressRepository.save(address);
    }

    public void delete(Long id) {
        this.addressRepository.deleteById(id);
    }
}
