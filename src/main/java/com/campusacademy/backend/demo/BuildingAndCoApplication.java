package com.campusacademy.backend.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BuildingAndCoApplication {

	public static void main(String[] args) {
		SpringApplication.run(BuildingAndCoApplication.class, args);
	}

}
