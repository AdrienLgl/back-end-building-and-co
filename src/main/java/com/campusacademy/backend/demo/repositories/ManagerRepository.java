package com.campusacademy.backend.demo.repositories;

import com.campusacademy.backend.demo.model.Manager;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ManagerRepository extends JpaRepository<Manager, Long> {

}
