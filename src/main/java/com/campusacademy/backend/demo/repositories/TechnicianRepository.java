package com.campusacademy.backend.demo.repositories;

import com.campusacademy.backend.demo.model.Technician;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TechnicianRepository extends JpaRepository<Technician, Long> {

}
