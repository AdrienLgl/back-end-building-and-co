package com.campusacademy.backend.demo.repositories;

import com.campusacademy.backend.demo.model.Vehicle;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(exported = true)
public interface VehicleRepository extends JpaRepository<Vehicle, Long> {

}
