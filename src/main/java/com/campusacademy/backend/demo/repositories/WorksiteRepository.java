package com.campusacademy.backend.demo.repositories;

import com.campusacademy.backend.demo.model.Worksite;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(exported = true)
public interface WorksiteRepository extends JpaRepository<Worksite, Long> {

}
