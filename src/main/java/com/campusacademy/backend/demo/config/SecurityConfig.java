package com.campusacademy.backend.demo.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  private final PasswordEncoder passwordEncoder;

  @Override
  public void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.inMemoryAuthentication()
        .withUser("technician").password(this.passwordEncoder.encode("technician")).roles("TECHNICIAN")
        .and()
        .withUser("manager").password(this.passwordEncoder.encode("manager")).roles("MANAGER")
        .and()
        .withUser("rh").password(this.passwordEncoder.encode("rh")).roles("RH")
        .and()
        .withUser("admin").password(this.passwordEncoder.encode("admin")).roles("ADMIN", "RH", "MANAGER", "TECHNICIAN");
  }

  @Override
  public void configure(HttpSecurity http) throws Exception {
    http.csrf().disable()
        // Connection httpBasic
        .httpBasic().and()
        // Connection formulaire
        .formLogin().and()
        .logout().deleteCookies("JSESSIONID").invalidateHttpSession(true).and()
        .authorizeRequests()
        .antMatchers(
            "/login",
            "/swagger-resources/**",
            "/swagger-ui/**",
            "/v2/api-docs",
            "/webjars/**")
        .permitAll()
        .antMatchers(HttpMethod.GET, "/technicians/**").hasRole("TECHNICIAN")
        .antMatchers(HttpMethod.GET, "/worksite/**").hasRole("MANAGER")
        .antMatchers(HttpMethod.POST, "/worksite/**").hasRole("MANAGER")
        .antMatchers(HttpMethod.PUT, "/worksite/**").hasRole("MANAGER")
        .antMatchers(HttpMethod.DELETE, "/worksite/**").hasRole("MANAGER")
        .antMatchers(HttpMethod.GET, "/manager/**", "/technicians/**").hasRole("RH")
        .antMatchers(HttpMethod.POST, "/manager/**", "/technicians/**").hasRole("RH")
        .antMatchers(HttpMethod.PUT, "/manager/**").hasRole("RH")
        .antMatchers(HttpMethod.DELETE, "/manager/**", "/technicians/**").hasRole("RH")
        .antMatchers(HttpMethod.GET, "/**").hasAnyRole("ADMIN")
        .antMatchers(HttpMethod.PUT, "/**").hasAnyRole("ADMIN")
        .antMatchers(HttpMethod.POST, "/**").hasAnyRole("ADMIN")
        .antMatchers(HttpMethod.DELETE, "/**").hasAnyRole("ADMIN")
        .anyRequest().authenticated();
  }

}
