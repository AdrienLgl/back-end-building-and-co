package com.campusacademy.backend.demo.model;

import javax.persistence.*;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "address")
@Data
@NoArgsConstructor
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "city_name", nullable = false, length = 100)
    private String cityName;

    @Column(name = "street", nullable = false, length = 100)
    private String street;

    @Column(name = "number")
    private int number;

    @OneToOne(mappedBy = "address")
    private Technician technician;

    @OneToOne(mappedBy = "address")
    private Worksite worksite;

}
