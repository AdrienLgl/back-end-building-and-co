package com.campusacademy.backend.demo.model;

import java.util.List;

import javax.persistence.*;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "manager")
@Data
@NoArgsConstructor
public class Manager {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; // long

    @Column(name = "manager_firstname", nullable = false, length = 100)
    private String managerFirstname;

    @Column(name = "manager_lastname", nullable = false, length = 100)
    private String managerLastname;

    @Column(name = "manager_fix_phone", nullable = false)
    private String managerFixPhone;

    @Column(name = "manager_portable_phone", nullable = false)
    private String managerPortablePhone;

    @OneToMany(mappedBy = "manager", cascade = CascadeType.PERSIST)
    private List<Technician> technicians;

}
