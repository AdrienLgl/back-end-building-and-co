package com.campusacademy.backend.demo.model;

import javax.persistence.*;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "vehicle")
@Data
@NoArgsConstructor
public class Vehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "vehicle_license", nullable = false, length = 100)
    private String vehicleLicense;

    @Column(name = "vehicle_brand", nullable = false, length = 100)
    private String vehicleBrand;

    @Column(name = "vehicle_year", nullable = false)
    private String vehicleYear;

    @OneToOne(mappedBy = "vehicle")
    private Technician technician;

}
